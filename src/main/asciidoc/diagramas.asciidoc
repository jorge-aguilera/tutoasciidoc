[[diagramas]]
= Diagramas

Una de las extensiones que más me gusta es la de
http://asciidoctor.org/docs/asciidoctor-diagram/[asciidoctor-diagram] que te
permite generar diagramas de alta calidad partiendo de un bloque de texto
asciidoctor.

== Diagrama "a mano"

Si escribimos en nuestro documento:

[#b10,source,Asciidoctor]
.diagramas/uno.adoc
----
include::diagramas/uno.asciidoc[]
----

Obtendremos como resultado:

include::diagramas/uno.asciidoc[]


== Casos de uso

[#b11,source,Asciidoctor]
----
include::diagramas/dos.asciidoc[]
----

include::diagramas/dos.asciidoc[]

== Diagrama de secuencias

[#b12,source,Asciidoctor]
----
include::diagramas/tres.asciidoc[]
----

include::diagramas/tres.asciidoc[]

== Maven

Para generar diagramas como los anteriores necesitamos modificar nuestro _pom.xml_
para incluir las dependencias necesarias tal como hicimos con la generación de
pdf:

[#b13,source,xml]
----
<plugin>
    <groupId>org.asciidoctor</groupId>
    <artifactId>asciidoctor-maven-plugin</artifactId>
    <version>1.5.3</version>
    <dependencies>
      <dependency>
        <groupId>org.asciidoctor</groupId>
        <artifactId>asciidoctorj</artifactId>
        <version>1.5.4.1</version>
      </dependency>
      <dependency>
         <groupId>org.asciidoctor</groupId>
         <artifactId>asciidoctorj-diagram</artifactId>
         <version>1.5.0</version>
     </dependency>
    </dependencies>
----

E indicar al plugin que utilice dicha extensión en la sección de configuración:

[source,xml]
----
<requires>
  <require>asciidoctor-diagram</require>
</requires>
----
