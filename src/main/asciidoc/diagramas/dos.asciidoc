
[plantuml,casos-de-uso-crud,png]
....
left to right direction
skinparam packageStyle rect
title Productos y lugares

actor Usuario as usuario

rectangle red{
    usuario -- (lugares)
    (lugares) <.. (alta lugar) : extend
    (lugares) <.. (modificacion lugar) : extend
    (lugares) <.. (ver lugar) : extend

    (productos) ...> (ver lugar)
    usuario -- (productos)
    (productos) <.. (alta producto) : extend
    (productos) <.. (modificacion producto) : extend
}

....
