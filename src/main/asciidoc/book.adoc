= Tutorial superbasico AsciiDoctor
jorge.aguilera@puravida-software.com
2017-01-01
:creator: {author}
:producer: Asciidoctor
:keywords: Asciidoctor, ebook, EPUB3, KF8, Tutorial
:copyright: CC-BY-SA 3.0
:revnumber: {version}
:revdate: {localdate}
:toc: macro
:doctype: book
:imagesdir: images
:lang: es
ifndef::ebook-format[:leveloffset: 1]
:source-highlighter: coderay
:stem:
:front-cover-image: image:portada.jpg[Portada,1050,1600]

:git-repo: https://jorge-aguilera.gitlab.io/tutoasciidoc

include::overview.asciidoc[]

include::intro.asciidoc[]

include::install.asciidoc[]

include::ejemplos.asciidoc[]

include::maven.asciidoc[]

include::gradle.asciidoc[]

include::html_pdf.asciidoc[]

include::anotaciones.asciidoc[]

include::diagramas.asciidoc[]

include::formulas.asciidoc[]
